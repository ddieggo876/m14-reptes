# Swarm / Nodes
Es poden desplegar aplicacions docker amb docker stack en un o més nodes que formen
un clúster que docker anomena swarm. Per defecte en els swarms es genera una xarxa
ingress  routing mesh que permet rèpliques de containers escoltant en un mateix port
i en diferents nodes. D'aquesta manera es poden desplegar aplicacions amb components
distribuïts entre diferents nodes.
Docker swarm genera per defecte una estructura de xarxa que permet a múltiples rèliques
per exemple d'un container web del port 80 escoltar en el port 80 del host amfitrió o
de cada un dels hosts (nodes) del swarm, estigui executant-s'hi el container o no.
El model de treball és:


- Amb docker swarm es pot crear un swarm, afegir i eliminar nodes d'un swarm.


- Amb docker nodes es pot gestionar l'estat d'un node, pausar-lo, etc.


- Amb docker stack es poden desplegar aplicacions en un swarm.


- Amb docker service o les configuracions dels serveis dins de docker-compose
es poden definir directives de col·locació dels serveis en els nodes.

# Docker Swarm

Iniciem el swarm:
~~~
$ docker swarm init

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-554xzefqaewv4b8meczn33zpb78v8rh9dskgolyb863biuqs30-apwpxf6itnls0o5t51gjdueek 10.200.243.203:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
~~~

Fem un join des de un altre ordinador, en concret desdel i21:
~~~
$ docker swarm join --token SWMTKN-1-554xzefqaewv4b8meczn33zpb78v8rh9dskgolyb863biuqs30-apwpxf6itnls0o5t51gjdueek 10.200.243.203:2377
~~~

# Colocacio de recursosals nodes

Inicialitzem myapp:
~~~
$ docker stack deploy -c docker-compose.yml myapp -> Desde Manager
Creating network myapp_webnet
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_portainer
Creating service myapp_web
~~~

Podem veure al visualizer com s'han organitzat els containers als diferents nodes.

Assignem labels als nodes






















