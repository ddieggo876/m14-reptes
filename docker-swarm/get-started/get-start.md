# GET STARTED WITH SWARM - REPTE 1
## Creem un Swarm
Iniciem el swarm al manager
~~~
$ docker swarm init
~~~
Inicia un nou cluster Swarm en el node (host) actual. Si vols especificar un node (host) que no es l'actual cal utilitzar la opció _--advertise-addr \<manager-ip>_:

~~~
# En aquest cas utilitzem el i20.
$ docker swarm init --advertise-addr 10.200.243.220 
~~~

## Mostrar info
Per llistar informacio detallada sobre l'entorn docker:

- Versió
- Contenidors
- Imatges
- Info sobre la xarxa
- Etc

~~~
$ docker info
~~~

## Afegir nodes

Es poden afegir nodes com a workers o altres managers amb aquestes ordres que mostren la ordre a executar: 
~~~
$ docker swarm join-token worker

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-304isaeugshcswx9n60lq4lmt0gxosrquv79g8y4lulakps4vq-8o7z86rhcraa2qlgxgeq515ly 10.200.243.220:2377
~~~

~~~
$ docker swarm join-token manager

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-304isaeugshcswx9n60lq4lmt0gxosrquv79g8y4lulakps4vq-8o7z86rhcraa2qlgxgeq515ly 10.200.243.220:2377
~~~
Aquestes ordres generen un toke que s'utilitza a la comanda que ja ve donada per afegir nous nodes al Swarm segons com els volguem si worker o manager.

## Llistar nodes

Per llistar els nodes fem:
~~~
$ docker node ls

ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
z9bsjwd7s2xdiwme3nbn9hcm9 *   i20        Ready     Active         Leader           24.0.7
myoelbs5mpw3ywszi45awi057     i21        Ready     Active                          24.0.2
~~~
Es necessari que fem l'ordre a un node manager

## Deploy a service

Posem en marcha una instancia utilitzan una imatge alpine:
~~~
docker service create --replicas 1 --name helloworld alpine ping docker.com

2sbgqqwxkwt6z7j1812hhdajq
overall progress: 1 out of 1 tasks 
1/1: running   [==================================================>] 
verify: Service converged
~~~
Creem un servei anomenada helloword a partir d'una imatge alpine i li diu que faci un ping a la pagina oficial de docker.

Per veure les intancies que hem replicat fem:
~~~
docker service ls
~~~

## Inspect de service

Per obtenir informacio mes detallada d'un servei que esta _up_ fem:
~~~
docker service inspect helloworld
~~~
Per mostrar la informacio en un format mes llegible podem posar la opcio "--pretty"
~~~
docker service inspect --pretty helloworld
~~~

## Scale

Per fer modificacions en calent, podem scalar el numero de repliques d'un serveri en el docker:
~~~
docker service scale helloword=15
~~~
Comprovem les intancies:
~~~
docker service ps helloworld
~~~
Es modifica el número de rèpliques del servei helloworld a 15, es despleguen en els
nodes. Es pot observar en el manager amb l'ordre docker service ps helloworld, en cada node es poden observar els containers (task) executant-se amb docker ps.

## Delete

Per aturar i esborrar un servei fem:
~~~
docker service rm helloword
~~~

Atura i esborra totes les intancies i serveis relacionats amb el serveri especificat.

## Rolling Updates

Pôdem actualitzar caracteristiques, versions, etc a un servei. Exemple, executem un servei redis amb 3 repliques i un retard d'actualitzacio de 10 segons i de versio "redis:3.0.6"

~~~
$ docker service create --replicas 3 --name redis --update-delay 10s redis:3.0.6

a9e0s47tpxvl19g14eg5bgy05
overall progress: 3 out of 3 tasks 
1/3: running   
2/3: running   
3/3: running   
verify: Service converged 
~~~

Per corroborar podem llistar totes les instancies del servei redis:

~~~
$ docker service ps redis
ID             NAME      IMAGE         NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
inz4td48jw4v   redis.1   redis:3.0.6   i21       Running         Running 45 seconds ago             
qwyyhatwejzw   redis.2   redis:3.0.6   i21       Running         Running 45 seconds ago             
rm9yru31oqau   redis.3   redis:3.0.6   i20       Running         Running 46 seconds ago 
~~~

Ara actualitzem la version de redis, en concret la imatge:

~~~
$ docker service update --image redis:3.0.7 redis
~~~

## Drain / pause node

Podem posar un node que estava active a estat drain, això fa que no accepti tasques i que se li eliminin les que executava passant-les a un altre node. Atenció, es tracta de tasques del swarm, el host pot executar tranquil·lament containers amb docker run i amb docker-compose.

~~~
$ docker node update --availability drain worker1
~~~

Podem tornar a activar el node fent:

~~~
$ docker node update --availability active worker1
~~~

Pausar un node fa que no acceti moves task però continua executant les que tenia
assignades