# Docker Swarm Deployment

Este documento proporciona instrucciones detalladas sobre cómo iniciar un swarm de Docker, desplegar un stack con servicios en el swarm, modificar la configuración del despliegue y gestionar servicios y nodos.


## Iniciar el Swarm
~~~
$ docker swarm init
~~~
Inicia un swarm y muestra las instrucciones para unir nodos al swarm como trabajadores o gerentes.


## Desplegar el Stack
~~~
$ docker stack deploy -c docker-compose.yml myapp
Creating network myapp_webnet
Creating service myapp_portainer
Creating service myapp_web
Creating service myapp_redis
Creating service myapp_visualizer
~~~
Despliega un stack con servicios en el swarm utilizando un archivo docker-compose.yml. Crea la red y servicios especificados en el archivo.

Llistem els contenidors que tenim activats:
~~~
$ docker stack ps myapp 
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
p2gbnrvmohlv   myapp_portainer.1    portainer/portainer:latest        debian    Running         Running 14 seconds ago             
p3a49tc0sbok   myapp_redis.1        redis:latest                      debian    Running         Running 17 seconds ago             
iaqabmj7m5ss   myapp_visualizer.1   dockersamples/visualizer:stable   debian    Running         Running 15 seconds ago             
ipimvrdgbe3y   myapp_web.1          kiliangp/getstarted:comptador     debian    Running         Running 17 seconds ago             
1r9t0t63rx4e   myapp_web.2          kiliangp/getstarted:comptador     debian    Running         Running 17 seconds ago
~~~
Llistem els serveis que tenim al stack myapp
~~~
$ docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
2ka7r9j425nr   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
1r819z9m5076   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
v6b3dn0flx96   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
pj4zsnjtkqac   myapp_web          replicated   3/3        edtasixm05/getstarted:comptador   *:80->80/tcp
~~~
Per aturar i esborrar els contenidors/serveis de l'aplicacio myapp:
~~~
docker stack rm myapp

Removing service myapp_portainer
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
~~~

## Modificar el Deploy
Obrim myapp amb la configuracio base:
~~~
$ docker stack deploy -c docker-compose.yml myapp
Creating network myapp_webnet
Creating service myapp_portainer
Creating service myapp_web
Creating service myapp_redis
Creating service myapp_visualizer
~~~
Editem el docker-compose.yml i pasem de 3 repliques a 5:
~~~
$ vim docker-compose.yml
~~~
Vuelve a desplegar el stack para aplicar cambios en la configuración del docker-compose.yml.
~~~
$ docker stack deploy -c docker-compose.yml myapp
Creating network myapp_webnet
Creating service myapp_web
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_portainer
~~~

Modificar el Número de Réplicas del Servicio Web
~~~
$ docker stack deploy -c docker-compose.yml  myapp
Updating service myapp_web (id: f56emr13car4hs8k8p68kplwr)
Updating service myapp_redis (id: gu5aihtu1isiqa6d3xbjscldk)
Updating service myapp_visualizer (id: tzb6m1vhesp5s2lmcyu885i62)
Updating service myapp_portainer (id: k0tt9mk3c7yrbhd83sqkc965o)
~~~
Actualiza el número de réplicas del servicio web según la configuración modificada en el archivo docker-compose.yml.


## Gestionar Servicios con Docker Service
~~~
docker service ls
~~~
Lista los servicios desplegados en el swarm con detalles como ID, nombre, modo, réplicas y puertos.
~~~
docker service scale myapp_web=4
~~~
Escala el número de réplicas del servicio web a 4.
Eliminar el Stack
~~~
docker stack rm myapp
~~~
Elimina el stack, servicios y la red asociada.


## Abandonar el Swarm
~~~
docker swarm leave --force
~~~
Abandona el swarm en el nodo actual.