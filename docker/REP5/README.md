# Repte 5

## Servidor LDAP amb Entrypoint (configurable)

Implementar una imatge de servidor ldap configurable segons el paràmetre rebut.
S’executa amb un entrypoint que és un script que actuarà segons li indiqui
l’argument rebut:

* initdb → ho inicialitza tot de nou i fa el populate de edt.org

* slapd → ho inicialitza tot però només engega el servidor, sense posar-hi
dades

* start / edtorg / res → engega el servidor utilitzant la persistència de dades de
la bd i de la configuració. És a dir, engega el servei usant les dades ja
existents

* slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada

Agafem els elements basic que hem utilitzat fins ara per realitzar els servidors ldap basic. El *edt-org.ldif*, el *slapd.conf* i els archius a personalitzar per al repte *Dockerfile* i *startup.sh*.

Configurem el *Dockerfile* per a que faci el mateix que ha fet sempre pero aquest cop que la execucio del *startup.sh* es realitzi sempre i que lo que especifiquem al final del docker run siguien opcions i no ordres. Aixo s'aconsegueix utilitzan l'ENTRYPOINT.

~~~
# ldapserver 2022
FROM debian:latest

LABEL version="1.0"
LABEL author="Diego Marcelo"
LABEL subject="ldapserver 2022"

# Establece la variable de entorno DEBIAN_FRONTEND para evitar interacciones
ENV DEBIAN_FRONTEND=noninteractive

# Instala los paquetes necesarios y define la contraseña del administrador
RUN apt-get update -y && \
    apt-get install -y procps iproute2 tree nmap vim slapd ldap-utils less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker

ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
EXPOSE 389
~~~

Posem com a accio principal l'execucio del archiu *startup.sh*, que ha de poder realitzar 4 accions diferents segons quin parametre utilitzi l'usuari.

~~~
#! /bin/bash

function initdb(){
 rm -rf /etc/ldap/slapd.d/*
 rm -rf /var/lib/ldap/*
 slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
 slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
 chown -R openldap.openldap /etc/ldap/slapd.d
 chown -R openldap.openldap /var/lib/ldap
 /usr/sbin/slapd -d0
}

function slapd(){
 rm -rf /etc/ldap/slapd.d/*
 rm -rf /var/lib/ldap/*
 slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
 chown -R openldap.openldap /etc/ldap/slapd.d
 chown -R openldap.openldap /var/lib/ldap
 /usr/sbin/slapd -d0
}

function edt(){
 /usr/sbin/slapd -d0

}

case $1 in
 "initdb")
  echo "initdb" && initdb
  ;;
 "slapd")
  echo "slapd" && slapd
  ;;
 "start")
  echo "start" && edt
  ;;
 "slapcat")
  if [ -n $2 ]; then
   slapcat -n $2
  else
   echo "Especifica la DB"
  fi
  ;;
esac
~~~

L'usuari pot especificar al docker run 4 accions que son les seguents:

* initdb: Engega el servidor ldap aplicant la configuracio del archiu designat (*slapd.conf*) i fent el populate de la DB desde zero.
* slapd: Engega el servidor ldap aplicant la configuracio del archiu designat (*slapd.conf*) pero no incerta les dades de la base de dades edt.org.
* start: Engega nomes el servidor ldap sense fer-ne res mes, utilitzant els archius dels volums com a base per treballar.
* slapcat [nº de BD a llistar]: fa un slapcat (llistar) de la BD especificada.

## Desplegament

Creacio dels volums:

~~~
docker volume create data
docker volume create conf
~~~

Creacio de la imatge sent al directori amb els archius designats:

~~~
docker build -t diegomarcelo/repte_5 .
~~~

Desplagament amb el *docker run* utilitzant els volums i en detach:

~~~
# Els 3 casos

# INITDB
docker run --rm --name cont1 -h edt.org -v conf:/etc/ldap/slapd.d/ -v data:/var/lib/ldap/ -d diegomarcelo/repte_5 initdb

#SLAPD
docker run --rm --name cont1 -h edt.org -v conf:/etc/ldap/slapd.d/ -v data:/var/lib/ldap/ -d diegomarcelo/repte_5 slapd

#Engegar amb peristencia de dades
docker run --rm --name cont1 -h edt.org -v conf:/etc/ldap/slapd.d/ -v data:/var/lib/ldap/ -d diegomarcelo/repte_5 start

#SLAPCAT
docker run --rm --name cont1 -h edt.org -v conf:/etc/ldap/slapd.d/ -v data:/var/lib/ldap/ -it diegomarcelo/repte_5 slapcat 1
~~~

Cuan relitzem un slapcat hem de fer-ho amb el container en "interactive" per poder veure la sortida de dades.
