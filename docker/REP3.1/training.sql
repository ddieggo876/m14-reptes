-- Crear una base de datos
CREATE DATABASE miBaseDeDatos;
\c mibasededatos;

-- Crear una tabla
CREATE TABLE MiTabla (
    ID INT PRIMARY KEY,
    Nombre VARCHAR(255),
    Edad INT,
    CorreoElectronico VARCHAR(255)
);

-- Insertar algunos datos en la tabla
INSERT INTO MiTabla (ID, Nombre, Edad, CorreoElectronico)
VALUES
    (1, 'Juan Pérez', 30, 'juan@example.com'),
    (2, 'María Rodríguez', 25, 'maria@example.com'),
    (3, 'Luis García', 35, 'luis@example.com');

