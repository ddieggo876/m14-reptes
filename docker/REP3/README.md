# Repte 3
Apunts: HowTo_ASIX_Docker.pdf capítol “Docker bàsic”.
 
 Usar postgres (la nostra versió) i una base de dades persistent. Usar la imatge postgres original amb populate i persistència de dades. Exemples de SQL injectat usant volumes

* Baixem la imatge postgres del docker hub:
  
  ~~~
  docker pull postgres:latest
  ~~~
* Creem el script sql de la nostra base de dades
* Fem el Dockerfile de la nostre imatge:

  ~~~
  FROM postgres:latest
  LABEL nom="repte3"
  LABEL author="Diego Marcelo"
  COPY training.sql /docker-entrypoint-initdb.d/
  ~~~
  El funcoinament del docker file es el de utilitzant la imatge postgres oficial copia un archiu .sql del directori actual i el posa al directori */docker-entrypoint-initdb.d*, que es el directori on la imatge postgres busca scripts o executables per posar usuaris i base de dades  predeterminats. 
* Constuim la imatge:
  ~~~
  docker build -t diegomarcelo/repte3 .
  ~~~
* Fem el volum per guardar les dades de forma persistent:
  ~~~
  docker volume create postvol
  ~~~
  Es pot fer "docker volume ps" per comprovar la correcta creacio del volum.

* Realitzar el docker run amb els parametres necesaris:
  ~~~
  docker run --name rep3
             -e POSTGRES_PASSWORD='jupiter'
             -v postvol:/var/lib/postgresql/data
             -d diegomarcelo/repte_3
  ~~~
  Li donem un nom al container amb --name. La opcio -e es obligatoria per al postgres, que es fa donarli una password al postgres mateix.
  Assignem un volum creat previament a la carpeta dades de postgres (-v).

  Es pot posar la opcio de variable de entorn -e POSTGRES_DB=nom_base_dades per crear una base de dades predeterminada


Ara podem crear tants containers amb postgres com volguem que la base de dades que tenim feta es mantindra.