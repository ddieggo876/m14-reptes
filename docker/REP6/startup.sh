#! /bin/bash

# export DEBIAN_FRONTEND=noninteractive
# apt-get -y install slapd
#
#Definim el nom del administrador
sed -i "s/Admin/$NOM_ADMIN/g" slapd.conf 
sed -i "s/passwd/$PASSWD/g" slapd.conf


rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

/usr/sbin/slapd -d0
